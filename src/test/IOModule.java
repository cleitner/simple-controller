/*
 *   Copyright 2013 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.nio.ByteBuffer;

import de.colin.timer.Clock;

public class IOModule extends Module {

    public IOModule(SimpleController controller, Clock clock, int slot,
            String name, int inputBytes, int outputBytes, int iodInputIndex,
            int iodOutputIndex) {
        super(controller, clock, slot, name, inputBytes, outputBytes,
                iodInputIndex, iodOutputIndex);

        setPanel(new IOPanel(name, slot, inputBytes * 8, outputBytes * 8));
    }

    @Override
    public void onIOInput(long timestamp, boolean dataGood, ByteBuffer data) {
        getPanel().updateInputs(dataGood, data);
    }

    @Override
    public boolean onIOOutput(long timestamp, ByteBuffer data) {
        return getPanel().getOutputs(data);
    }

    @Override
    public void onIOTimeout(long timestamp) {
    }
}
