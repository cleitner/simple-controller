/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.Callable;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.xml.sax.InputSource;

import de.colin.profinet.AlarmPriority;
import de.colin.profinet.Controller;
import de.colin.profinet.ControllerHandler;
import de.colin.profinet.config.ConfigurationVisitor;
import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.config.ModuleConfiguration;
import de.colin.profinet.config.ParameterRecordConfiguration;
import de.colin.profinet.config.SubmoduleConfiguration;
import de.colin.profinet.config.XmlConfigurationParser;
import de.colin.profinet.io.Frame;
import de.colin.profinet.io.IODataObject;
import de.colin.profinet.pnrt.DCPInfo;
import de.colin.timer.DefaultClock;

@SuppressWarnings("serial")
public class SimpleController extends JFrame implements Callable<Integer>,
        ControllerHandler {

    public static void main(String[] args) throws Exception {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (InstantiationException e) {
        } catch (ClassNotFoundException e) {
        } catch (UnsupportedLookAndFeelException e) {
        } catch (IllegalAccessException e) {
        }

        if (args.length != 3) {
            System.out.printf("usage: %s <local-ip> <config.xml> <fb-name>%n",
                    SimpleController.class.getName());
            System.exit(1);
            return;
        }

        InetAddress localAddress = InetAddress.getByName(args[0]);
        String fbName = args[2];
        DeviceConfiguration config = XmlConfigurationParser
                .parse(new InputSource(args[1]));

        SimpleController sc = new SimpleController(localAddress, fbName, config);
        sc.setVisible(true);
        sc.call();
    }

    private final InetAddress localAddress;
    private final ConsoleFrame console;
    private final JPanel vPanel = new JPanel();

    private final SortedMap<Integer, Module> modules = new TreeMap<Integer, Module>();

    private final String fbName;
    private final DeviceConfiguration config;

    private final DefaultClock clock;
    private final Controller controller;

    @SuppressWarnings("unchecked")
    private <M extends Module> M getModule(int slot) {
        return (M) modules.get(slot);
    }

    private void connect() {
        try {
            if (controller.isConnected()) {
                DCPInfo info = controller.findStationByName(fbName);
                if (info == null)
                    return;
                controller.signalStation(info.getPNRTAddress());
                return;
            }

            printf("Connecting to \"" + fbName + "\"...");

            DCPInfo info = controller.findStationByName(fbName);
            if (info == null) {
                printf("Couldn't find terminal \"" + fbName + "\"");
                return;
            }

            InetAddress ip = info.getIPAddress();

            // No IP?
            if (ip == null) {
                InetAddress netmask = InetAddress.getByName("255.255.255.0");

                Inet4Address localV4Address = (Inet4Address) localAddress;
                // Lets try to assign x.x.x.239
                ip = InetAddress.getByAddress(new byte[] {
                        localV4Address.getAddress()[0],
                        localV4Address.getAddress()[1],
                        localV4Address.getAddress()[2], (byte) 239 });

                printf("Terminal has no IP. Trying to set %s", ip);

                controller.setStationIPAddress(info.getPNRTAddress(), ip,
                        netmask, InetAddress.getByName("0.0.0.0"), false);
            } else {
                printf("Terminal has IP %s", ip);
            }

            controller.signalStation(info.getPNRTAddress());

            vPanel.removeAll();
            for (Module m : modules.values()) {
                m.dispose();
            }
            modules.clear();

            controller.connect(new InetSocketAddress(ip, 0x8894), 0x8100,
                    2 + (int) (Math.random() * 10), 128, config);

            for (Module m : modules.values()) {
                ModuleConfiguration moduleConfig = config
                        .getModule(m.getSlot());
                for (SubmoduleConfiguration submoduleConfig : moduleConfig
                        .getSubmodules()) {
                    for (ParameterRecordConfiguration paramRecord : submoduleConfig
                            .getParameterRecords()) {
                        controller.write(0, m.getSlot(),
                                submoduleConfig.getSubslot(),
                                paramRecord.getIndex(),
                                ByteBuffer.wrap(paramRecord.getData()));
                    }
                }

                // Finally let the panels do their magic
                m.writeConfig(controller);
            }

            controller.parameterEnd();

        } catch (IOException e) {
            printf("Failed to connect: " + e);
        }
    }

    public SimpleController(InetAddress localAddress, String fbName,
            DeviceConfiguration config) {
        this.localAddress = localAddress;
        this.fbName = fbName;
        this.config = config;

        console = new ConsoleFrame();

        clock = new DefaultClock();
        clock.update();

        controller = new Controller("simple", this);

        vPanel.setLayout(new BoxLayout(vPanel, BoxLayout.PAGE_AXIS));

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationByPlatform(true);

        getContentPane().add(vPanel);
    }

    @Override
    public Integer call() throws IOException {
        controller.start(new InetSocketAddress(localAddress, 0x8894), false);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                connect();
            }
        }, 0, 1000);

        return 0;
    }

    public void printf(String fmt, Object... args) {
        System.out.printf(fmt + "\n", args);
        console.printf(fmt + "\n", args);
    }

    // -- Profinet --

    @Override
    public void onConnect(Controller controller) {
        printf("PROFINET Connected");

        final Frame inputFrame = controller.getInputFrame();
        final Frame outputFrame = controller.getOutputFrame();

        DeviceConfiguration actualConfig = controller.getDeviceConfiguration();

        actualConfig.accept(new ConfigurationVisitor() {
            @Override
            public void visitParameterRecord(ParameterRecordConfiguration record) {
            }

            @Override
            public void visitSubmodule(SubmoduleConfiguration submodule) {
            }

            @Override
            public void visitModule(ModuleConfiguration module) {

                Module m = null;
                int slot = module.getSlot();
                int id = module.getIdentNumber();
                int iodInputIndex = inputFrame.findIODIndex(slot, 1);
                int iodOutputIndex = outputFrame.findIODIndex(slot, 1);

                printf("Module 0x%08X in slot %d", id, slot);

                if (id == 0x60608C1) {
                    // FVDA-P2

                    printf("FVDA-P2 in slot " + slot);

                    m = new SafetyModule(SimpleController.this, clock, slot,
                            "FVDA-P2",
                            module.getSubmodule(1).getInputLength() - 4, module
                                    .getSubmodule(1).getOutputLength() - 4,
                            iodInputIndex, iodOutputIndex);

                } else if (id == 0x60601C1) {
                    // FVDA-P

                    printf("FVDA-P in slot " + slot);

                    m = new SafetyModule(SimpleController.this, clock, slot,
                            "FVDA-P",
                            module.getSubmodule(1).getInputLength() - 4, module
                                    .getSubmodule(1).getOutputLength() - 4,
                            iodInputIndex, iodOutputIndex);

                } else if (module.getIdentNumber() == 0x3038011C) {
                    // F8DE-P

                    printf("F8DE-P in slot " + slot);

                    m = new SafetyModule(SimpleController.this, clock, slot,
                            "F8DE-P",
                            module.getSubmodule(1).getInputLength() - 4, module
                                    .getSubmodule(1).getOutputLength() - 4,
                            iodInputIndex, iodOutputIndex);

                } else {
                    SubmoduleConfiguration submodule = module.getSubmodule(1);
                    if (submodule != null
                            && (submodule.getInputLength() > 0 || submodule
                                    .getOutputLength() > 0)) {

                        m = new IOModule(SimpleController.this, clock, slot,
                                null, submodule.getInputLength(), submodule
                                        .getOutputLength(), iodInputIndex,
                                iodOutputIndex);
                    }
                }

                if (m != null) {
                    modules.put(slot, m);
                    vPanel.add(m.getPanel());
                }
            }

            @Override
            public void visitDevice(DeviceConfiguration device) {
            }
        });

        pack();
    }

    @Override
    public void onApplicationReady(Controller controller) {
        printf("PROFINET Application Ready");
    }

    @Override
    public void onDisconnect(Controller controller) {
        printf("PROFINET Disconnected");

        // psHost.restart();
    }

    @Override
    public void onAlarmAppears(Controller controller, AlarmPriority priority,
            int slot, int subslot, int channel, int errorType) {
        printf("PROFINET Alarm appears: priority %s, slot %d/%d, channel %d: %d (0x%04X)",
                priority, slot, subslot, channel, errorType, errorType);
    }

    @Override
    public void onAlarmDisappears(Controller controller,
            AlarmPriority priority, int slot, int subslot, int channel) {
        printf("PROFINET Alarm disappears: priority %s, slot %d/%d, channel %d",
                priority, slot, subslot, channel);
    }

    @Override
    public void onIOOutput(Controller controller, long timestamp, Frame frame) {
        clock.update();

        for (IODataObject iod : frame.getIODataObjects()) {
            Module m = getModule(iod.getSlot());

            if (m == null) {
                continue;
            }

            if (m.getIODOutputIndex() != -1) {
                frame.setDataGood(
                        m.getIODOutputIndex(),
                        m.onIOOutput(timestamp,
                                frame.dataSlice(m.getIODOutputIndex())));
            }
        }
    }

    @Override
    public void onIOInput(Controller controller, long timestamp, Frame frame) {
        clock.update();

        for (IODataObject iod : frame.getIODataObjects()) {
            Module m = getModule(iod.getSlot());

            if (m == null) {
                continue;
            }

            if (m.getIODInputIndex() != -1) {
                m.onIOInput(timestamp, frame.isDataGood(m.getIODInputIndex()),
                        frame.dataSlice(m.getIODInputIndex()));
            }
        }
    }

    @Override
    public void onIOTimeout(Controller controller, long timestamp) {
        printf("PROFINET I/O timeout");
    }

    @Override
    public void onStationChanged(Controller controller, SocketAddress station,
            DCPInfo info) {
        printf("DCP station changed: %s, %s", station, info);
    }

    @Override
    public void onStationDetected(Controller controller, SocketAddress station,
            DCPInfo info) {
        printf("DCP station detected: %s, %s", station, info);
    }

    @Override
    public void onStationLost(Controller controller, SocketAddress station) {
        printf("DCP station lost: %s", station);
    }
}
