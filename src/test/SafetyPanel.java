/*
 *   Copyright 2013 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.awt.Color;

import javax.swing.BorderFactory;

@SuppressWarnings("serial")
public class SafetyPanel extends IOPanel {

    public SafetyPanel(String name, int slot, int inputs, int outputs) {
        super(name, slot, inputs * 8, outputs * 8);

        outputsGoodChkBox.setText("OA");

        setBorder(BorderFactory.createLineBorder(new Color(0xFFEE00), 5));
    }

}
