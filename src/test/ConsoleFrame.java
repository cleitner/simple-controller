/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class ConsoleFrame extends JFrame {

    private final JEditorPane pane;

    private void print(String txt) {
        pane.setText(pane.getText() + txt);
    }

    public ConsoleFrame() {

        pane = new JEditorPane();
        pane.setFont(new Font("Monospaced", Font.PLAIN, 10));

        JScrollPane scroll = new JScrollPane(pane,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        getContentPane().add(scroll);

        setLocationByPlatform(true);
        setPreferredSize(new Dimension(400, 300));

        pack();
    }

    public void printf(String fmt, Object... args) {
        final String txt = String.format(fmt, args);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (!isVisible()) {
                    setVisible(true);
                }
                print(txt);
            }
        });
    }

}
