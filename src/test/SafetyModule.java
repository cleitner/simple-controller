/*
 *   Copyright 2013 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.io.IOException;
import java.nio.ByteBuffer;

import de.colin.profinet.Controller;
import de.colin.profisafe.ProfisafeHost;
import de.colin.profisafe.ProfisafeHostHandler;
import de.colin.timer.Clock;

public class SafetyModule extends Module implements ProfisafeHostHandler {

    private final ProfisafeHost psHost;

    public SafetyModule(SimpleController controller, Clock clock, int slot,
            String name, int inputBytes, int outputBytes, int iodInputIndex,
            int iodOutputIndex) {
        super(controller, clock, slot, name, inputBytes, outputBytes,
                iodInputIndex, iodOutputIndex);

        psHost = new ProfisafeHost(clock, inputBytes, outputBytes, this);

        psHost.setHostAddress(slot + 1);

        psHost.setDeviceAddress(0xF);

        psHost.setPrmFlag1(0x08);
        psHost.setPrmFlag2(0x40);

        psHost.setWatchdogTimeout(1000);
        psHost.setProcessOutput(new byte[outputBytes]);
        psHost.restart();

        setPanel(new SafetyPanel(name, slot, inputBytes, outputBytes));
    }

    @Override
    public void dispose() {
        psHost.stop();
    }

    @Override
    public void writeConfig(Controller controller) throws IOException {
        if (getName().equals("FVDA-P") || getName().equals("FVDA-P2")) {
            writeFVDAConfig(controller, getSlot(), psHost);
        } else if (getName().equals("F8DE-P")) {
            writeF8DEConfig(controller, getSlot(), psHost);
        } else {
            printf("Unknown safety module \"%s\"", getName());
        }
    }

    public ProfisafeHost getProfisafeHost() {
        return psHost;
    }

    @Override
    public void onWatchdogTimeout(ProfisafeHost host) {
        printf("PROFIsafe Host Watchdog timeout on slot %d", getSlot());
    }

    @Override
    public void onConnected(ProfisafeHost host) {
        printf("PROFIsafe Host connected on slot %d", getSlot());
    }

    @Override
    public void onCRCError(ProfisafeHost host) {
        printf("PROFIsafe Host detected CRC error on slot %d", getSlot());
    }

    @Override
    public void onFVActivated(ProfisafeHost host) {
        printf("PROFIsafe uses FV on slot %d", getSlot());
    }

    @Override
    public void onOANecessary(ProfisafeHost host) {
        printf("PROFIsafe Host needs Operator Acknowledgement on slot %d",
                getSlot());
    }

    @Override
    public void onIOInput(long timestamp, boolean dataGood, ByteBuffer data) {
        if (dataGood) {
            psHost.readProfisafeInput(data);

            byte[] input = new byte[psHost.getProcessInputLength()];
            psHost.getProcessInput(input);

            getPanel().updateInputs(true, ByteBuffer.wrap(input));
        } else {
            getPanel().updateInputs(false,
                    ByteBuffer.wrap(new byte[psHost.getProcessInputLength()]));
        }
    }

    @Override
    public boolean onIOOutput(long timestamp, ByteBuffer data) {
        boolean oa;

        byte[] output = new byte[psHost.getProcessOutputLength()];

        oa = getPanel().getOutputs(ByteBuffer.wrap(output));

        psHost.setProcessOutput(output);
        psHost.setOA(oa);

        psHost.writeProfisafeOutput(data);

        return true;
    }

    @Override
    public void onIOTimeout(long timestamp) {
    }

    private static void writeFVDAConfig(Controller controller, int slot,
            ProfisafeHost psHost) throws IOException {
        controller.write(
                0,
                slot,
                1,
                1,
                ByteBuffer.wrap(new byte[] { (byte) 0x01, (byte) 0x00,
                        (byte) 0x08, (byte) 0x04, (byte) 0x00, (byte) 0x00,
                        (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x7F,
                        (byte) 0x00, }));

        controller.write(
                0,
                slot,
                1,
                2,
                ByteBuffer.wrap(new byte[] { (byte) 0x01, (byte) 0x06,
                        (byte) 0x01, (byte) 0x7F, }));

        controller.write(
                0,
                slot,
                1,
                3,
                ByteBuffer.wrap(new byte[] { (byte) 0x01, (byte) 0x07,
                        (byte) 0x01, (byte) 0x00, (byte) 0x41, (byte) 0x0C,
                        (byte) 0x0A, (byte) 0xFF }));

        controller.write(
                0,
                slot,
                1,
                0x3e9,
                ByteBuffer.wrap(new byte[] { (byte) psHost.getPrmFlag1(),
                        (byte) psHost.getPrmFlag2(),
                        (byte) (psHost.getHostAddress() >> 8),
                        (byte) (psHost.getHostAddress()),
                        (byte) (psHost.getDeviceAddress() >> 8),
                        (byte) (psHost.getDeviceAddress()),
                        (byte) (psHost.getWatchdogTimeout() >> 8),
                        (byte) (psHost.getWatchdogTimeout()),
                        (byte) (psHost.getCRC1() >> 8),
                        (byte) (psHost.getCRC1()), }));
    }

    private static void writeF8DEConfig(Controller controller, int slot,
            ProfisafeHost psHost) throws IOException {
        controller.write(0, slot, 1, 1, ByteBuffer.wrap(new byte[] {
                (byte) 0x01, (byte) 0x06, (byte) 0x02, (byte) 0xFB,
                (byte) 0x34, (byte) 0x41, (byte) 0x0C, (byte) 0x0A,
                (byte) 0xFF, 0, 0, 0, 0, 0, 0 }));

        controller.write(
                0,
                slot,
                1,
                0x3e9,
                ByteBuffer.wrap(new byte[] { (byte) psHost.getPrmFlag1(),
                        (byte) psHost.getPrmFlag2(),
                        (byte) (psHost.getHostAddress() >> 8),
                        (byte) (psHost.getHostAddress()),
                        (byte) (psHost.getDeviceAddress() >> 8),
                        (byte) (psHost.getDeviceAddress()),
                        (byte) (psHost.getWatchdogTimeout() >> 8),
                        (byte) (psHost.getWatchdogTimeout()),
                        (byte) (psHost.getCRC1() >> 8),
                        (byte) (psHost.getCRC1()), }));
    }
}
