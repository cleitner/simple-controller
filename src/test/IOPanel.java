/*
 *   Copyright 2012 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.nio.ByteBuffer;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

@SuppressWarnings("serial")
public class IOPanel extends JPanel {

    private final int inputs;

    private boolean outputsGood;
    private byte[] outputData;
    protected JCheckBox outputsGoodChkBox;

    private JRadioButton inputsGoodBtn;
    private JRadioButton[] inputBtns;

    public IOPanel(String name, int slot, int inputs, int outputs) {
        this.inputs = inputs;

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        JLabel nameLbl = new JLabel();
        nameLbl.setFont(getFont().deriveFont(Font.BOLD));
        if (name != null) {
            nameLbl.setText(name + " (slot " + slot + ")");
        } else {
            nameLbl.setText("Slot " + slot);
        }
        add(nameLbl);

        if (inputs > 0) {
            JPanel inputsPanel = new JPanel();
            inputsPanel.setLayout(new FlowLayout());

            inputsGoodBtn = new JRadioButton("Good");
            inputsGoodBtn.setEnabled(false);
            inputsPanel.add(inputsGoodBtn);

            inputBtns = new JRadioButton[inputs];
            for (int n = 0; n < inputs; n++) {
                if ((n % 8) == 0) {
                    JPanel div = new JPanel();
                    div.setPreferredSize(new Dimension(3, 12));
                    div.setBackground(Color.GRAY);
                    inputsPanel.add(div);

                    inputsPanel.add(new JLabel("" + n));
                }

                inputBtns[n] = new JRadioButton(/* "" + (n % 8) */);
                inputBtns[n].setEnabled(false);
                inputsPanel.add(inputBtns[n]);
            }

            add(inputsPanel);
        }

        if (outputs > 0) {
            outputData = new byte[outputs / 8 + ((outputs % 8 != 0) ? 1 : 0)];

            JPanel outputsPanel = new JPanel();
            outputsPanel.setLayout(new FlowLayout());

            outputsGoodChkBox = new JCheckBox(new AbstractAction("Good") {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JCheckBox btn = (JCheckBox) e.getSource();
                    synchronized (IOPanel.this) {
                        outputsGood = btn.isSelected();
                    }
                }
            });
            outputsPanel.add(outputsGoodChkBox);

            for (int n = 0; n < outputs; n++) {
                final int output = n;

                if ((n % 8) == 0) {
                    JPanel div = new JPanel();
                    div.setPreferredSize(new Dimension(3, 12));
                    div.setBackground(Color.GRAY);
                    outputsPanel.add(div);

                    outputsPanel.add(new JLabel("" + n));
                }

                JCheckBox outputChkBox = new JCheckBox(new AbstractAction(/*
                                                                           * ""
                                                                           * +
                                                                           * (n
                                                                           * %
                                                                           * 8)
                                                                           */) {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JCheckBox cb = (JCheckBox) e.getSource();
                        synchronized (IOPanel.this) {
                            if (cb.isSelected()) {
                                outputData[output / 8] |= 1 << (output % 8);
                            } else {
                                outputData[output / 8] &= ~(1 << (output % 8));
                            }
                        }
                    }
                });
                outputsPanel.add(outputChkBox);
            }

            add(outputsPanel);
        }
    }

    public void updateInputs(final boolean good, ByteBuffer data) {
        int N = inputs / 8;
        if (inputs % 8 != 0) {
            N += 1;
        }

        if (N == 0) {
            return;
        }

        final byte[] bytes = new byte[N];

        data.position(0);
        data.get(bytes);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                inputsGoodBtn.setSelected(good);

                for (int n = 0; n < inputs; n++) {
                    inputBtns[n]
                            .setSelected((bytes[n / 8] & (1 << (n % 8))) != 0);
                }
            }
        });
    }

    public synchronized boolean getOutputs(ByteBuffer data) {
        data.position(0);
        data.put(outputData);

        return outputsGood;
    }
}
