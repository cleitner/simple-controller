/*
 *   Copyright 2013 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package test;

import java.io.IOException;
import java.nio.ByteBuffer;

import de.colin.profinet.Controller;
import de.colin.timer.Clock;

public abstract class Module {

    private final Clock clock;
    private final int slot;
    private final String name;
    private final int inputBytes;
    private final int outputBytes;
    private final int iodInputIndex;
    private final int iodOutputIndex;
    private final SimpleController controller;
    private IOPanel panel;

    protected Module(SimpleController controller, Clock clock, int slot,
            String name, int inputBytes, int outputBytes, int iodInputIndex,
            int iodOutputIndex) {

        this.controller = controller;
        this.clock = clock;
        this.slot = slot;
        this.name = name;
        this.inputBytes = inputBytes;
        this.outputBytes = outputBytes;
        this.iodInputIndex = iodInputIndex;
        this.iodOutputIndex = iodOutputIndex;
    }

    public void dispose() {
    }

    public void writeConfig(Controller controller) throws IOException {
    }

    public SimpleController getController() {
        return controller;
    }

    public Clock getClock() {
        return clock;
    }

    public String getName() {
        return name;
    }

    public int getProcessInputBytes() {
        return inputBytes;
    }

    public int getProcessOutputBytes() {
        return outputBytes;
    }

    protected void setPanel(IOPanel panel) {
        this.panel = panel;
    }

    public IOPanel getPanel() {
        return panel;
    }

    public final int getSlot() {
        return slot;
    }

    public final int getIODInputIndex() {
        return iodInputIndex;
    }

    public final int getIODOutputIndex() {
        return iodOutputIndex;
    }

    protected void printf(String fmt, Object... args) {
        controller.printf(fmt, args);
    }

    public abstract boolean onIOOutput(long timestamp, ByteBuffer data);

    public abstract void onIOInput(long timestamp, boolean dataGood,
            ByteBuffer data);

    public abstract void onIOTimeout(long timestamp);
}
